<?php


namespace Noa\Parser;


use Noa\Parser\Components\Group;
use Noa\Parser\Components\LogicalOperator;

class ReversePolishNotation {

    /**
     * http://www.learn4master.com/data-structures/stack/convert-infix-notation-to-reverse-polish-notation-java
     * @param Pair $pair
     * @return array
     */
    public static function from(Pair $pair) {

        $operatorStack = [];
        $left = $pair->left();
        $result = [];

        foreach ($left as $component) {


            switch ($component) {
                case $component instanceof LogicalOperator:
                    $operatorStack[] = $component;
                    break;
                case $component instanceof Group:
                    $rpn = self::from($component->getParsed()->get());
                    $result = array_reduce($rpn, function($acc, $operator) {
                        $acc[] = $operator;
                        return $acc;
                    }, $result);
                    break;
                default:
                    $result[] = $component;
                    break;
            }
        };

        return array_reduce($operatorStack, function($acc, $operator) {
            $acc[] = $operator;
            return $acc;
        }, $result);
    }

}