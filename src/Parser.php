<?php


namespace Noa\Parser;


use Closure;
use Noa\Parser\Components\ComponentInterface;

final class Parser
{

    const MODIFIER_ALL = "*";
    const MODIFIER_AT_LEAST_ONE = "+";
    const MODIFIER_AT_MOST_ONE = "?";


    /**
     * @param $query
     * @codeCoverageIgnore Not yet implemented
     */
    public function parse($query) {

    }

    /**
     * Returns a combined parser that matches all the tokens
     * if one of token is not matched return Nothing, otherwise the array of token matched
     * @param string ...$tokens
     * @return Closure
     */
    public static function allOfLiteral(...$tokens) {

        $parsers = array_map(function($token) {
            return Parser::matchLiteral($token);
        }, $tokens);

        return self::allOf(...$parsers);
    }


    /**
     * Returns a combined parser that match all of the parsers
     * if one of the parser not match Nothing will be return
     * @param string ...$parsers
     * @return Closure
     */
    public static function allOf(...$parsers) {
        return function ($string) use ($parsers) {
            if (count($parsers) === 0) {
                return Result::ok(new Pair([], $string));
            }

            if (count($parsers) === 1) {
                return self::run($parsers[0], $string);
            }

            $first = array_shift($parsers);
            $combinedParser = array_reduce($parsers, function($previous, $parser) {
                $combinedParser = self::andThen($previous, $parser);
                return $combinedParser;
            }, $first);

            $result =  self::run($combinedParser, $string);
            if ($result instanceof Error) {
                return $result;
            }
            $value = $result->get()->left();
            // Recursively flatten the left result
            array_walk_recursive($value, function ($v) use(&$flatten) {
                $flatten[] = $v;
            });
            return Result::ok(new Pair($flatten, $result->get()->right()));
        };
    }

    /**
     * Returns a combined parser that matches one of the tokens
     * if none of the tokens are not matched return Nothing, otherwise the matched token
     * @param string ...$tokens
     * @return Closure
     */
    public static function oneOfLiteral(...$tokens) {

        $parsers = array_map(function($token) {
            return Parser::matchLiteral($token);
        }, $tokens);

        return self::oneOf(...$parsers);
    }

    /**
     * Returns a combined parser that match one of the parser
     * if all of the parsers not match Nothing will be return
     * @param Closure ...$parsers
     * @return Closure
     */
    public static function oneOf(...$parsers) {
        return function ($string) use ($parsers) {
            if (count($parsers) === 0) {
                return Result::ok(new Pair([], $string));
            }

            if (count($parsers) === 1) {
                return self::run($parsers[0], $string);
            }

            $first = array_shift($parsers);
            $combinedParsers = array_reduce($parsers, function($previous, $parser) {
                $combinedParser = self::orElse($previous, $parser);
                return $combinedParser;
            }, $first);

            $result =  self::run($combinedParsers, $string);
            if ($result instanceof Error) {
                return $result;
            }
            return $result;
        };
    }

    /**
     * Create a parser from the given $parser with its return decorated by f function
     * example: if f convert to int and parser return Just("123") then f(run(parser, "123")) = Just(123)
     * @param $parser
     * @param string|array $f Takes a Maybe
     * @return Closure
     */
    public static function apply($parser, $f) {
        return function ($string) use($parser, $f) {
            $result = Parser::run($parser, $string);
            if ($result instanceof Error) {
                return $result;
            }
            return call_user_func($f, $result);
        };
    }


    /**
     * Check if array could be concatenated
     * @param $input
     * @return bool
     */
    private static function validateCouldBeConcat($input) {

        if (!is_array($input)) {
            return false;
        }

        if (count($input) === 0) {
            return true;
        }

        return count(array_filter($input, function($element) {
            return !is_string($element) && !($element instanceof ComponentInterface);
        })) === 0;
    }


    /**
     * Try concatenate array of string
     * @param Ok|Error $input
     * @return Ok|Error
     */
    public static function concatStrings($input) {

        if ($input instanceof Error) {
            return $input;
        }

        $justData = $input->get();

        if($justData instanceof Pair) {

            $value = $justData->left();
            $right = $justData->right();

            if (is_string($value)) {
                return Result::ok(new Pair($value, $right));
            }

            if (is_array($value) && count($value)) {
                array_walk_recursive($value, function ($v) use (&$flatten) {
                    $flatten[] = $v;
                });
                $value = $flatten;
            }

            if (!self::validateCouldBeConcat($value)) {
                return Result::error();
            }

            $left = implode("", $value);
            return Result::ok(new Pair($left, $right));
        }

        if (is_string($justData)) {
            return Result::ok($justData);
        }

        if (is_array($justData) && count($justData)) {
            array_walk_recursive($justData, function ($v) use(&$flatten) {
                $flatten[] = $v;
            });
            $justData = $flatten;
        }

        if (!self::validateCouldBeConcat($justData)) {
            return Result::error();
        }

        return Result::ok(implode("", $justData));
    }

    /**
     * Try to convert a value to numeric
     * @param Ok|Error $input
     * @param string|array $f The method to apply to convert the numeric string to numeric
     * @return Ok|Error
     */
    private static function fromNumeric($input, $f) {

        if ($input instanceof Error) {
            return $input;
        }

        $justData = $input->get();

        if($justData instanceof Pair) {

            $value = $justData->left();

            if (!is_numeric($value)) {
                return Result::error();
            }

            $left = call_user_func($f, $value);
            $right = $justData->right();
            return Result::ok(new Pair($left, $right));
        }

        if (!is_numeric($justData)) {
            return Result::error();
        }

        return Result::ok(call_user_func($f, $justData));
    }

    /**
     * Convert a numeric string to an int
     * if the value is Nothing or not numeric returns Nothing
     * if the value is a Tuple returns a Tuple with the left value converted as integer
     * @param Ok|Error $input
     * @return mixed
     */
    public static function toInt($input) {

        return self::fromNumeric($input, 'intval');
    }

    /**
     * Convert a numeric string to an float
     * if the value is Nothing or not numeric returns Nothing
     * if the value is a Tuple returns a Tuple with the left value converted as float
     * @param Ok|Error $input
     * @return mixed
     */
    public static function toFloat($input) {

        return self::fromNumeric($input, 'floatval');
    }

    /**
     * Combines the two parsers
     * Workflow:
     *  1 - runs the first parser
     *  2 - if the result is not Nothing return the result of the first parser
     *  3 - otherwise, runs the second parser
     *  4 - if the result is not Nothing returns the result of the second parser
     *  5 - otherwise returns Nothing
     * @param Closure $parser1
     * @param Closure $parser2
     * @return Closure
     */
    public static function orElse($parser1, $parser2) {
        return function ($string) use ($parser1, $parser2) {
            $result1 = self::run($parser1, $string);
            if ($result1 instanceof Ok) {
                return $result1;
            }
            $result2 = self::run($parser2, $string);
            if ($result2 instanceof Ok) {
                return $result2;
            }
            return Result::error();
        };
    }

    /**
     * Combines the two parsers
     * 1 - runs the first parser
     * 2 - if the result is Nothing, returns Nothing
     * 3 - otherwise, runs the second parser with the left string after parser1 running
     * 4 - if the result is Nothing, returns Nothing
     * 5 - otherwise, returns a Just Tuple with as left value the concatenated left values of first and second parser,
     * and as right the left string after parser 2 running
     * @param $parser1
     * @param $parser2
     * @return Closure
     */
    public static function andThen($parser1, $parser2) {
        return function ($string) use($parser1, $parser2) {
            $result1 = self::run($parser1, $string);
            if ($result1 instanceof Error) {
                return $result1;
            }
            $result2 = self::run($parser2, $result1->get()->right());
            if ($result2 instanceof Error) {
                return $result2;
            }
            $left = [$result1->get()->left(), $result2->get()->left()];
            $right = $result2->get()->right();
            return Result::ok(new Pair($left, $right));
        };
    }

    /**
     * Return a parser that matches only digit characters
     * @return Closure
     */
    public static function matchDigit() {
        return self::oneOfLiteral(...range('0', '9'));
    }

    /**
     * Return a parser that matches only lower case letter characters
     * @return Closure
     */
    public static function matchLowerCaseLetter() {
        return self::oneOfLiteral(...range('a', 'z'));
    }

    /**
     * Return a parser that matches only upper case letter characters
     * @return Closure
     */
    public static function matchUpperCaseLetter() {
        return self::oneOfLiteral(...range('A', 'Z'));
    }

    /**
     * Return a parser that matches all letter no care about the case
     * @return Closure
     */
    public static function matchLetter() {
        return self::oneOf(...[self::matchUpperCaseLetter(), self::matchLowerCaseLetter()]);
    }

    /**
     * Return a parser that matches all alpha numeric character no care about the case
     * @return Closure
     */
    public static function matchAlphaNumeric() {
        return self::oneOf(...[self::matchLetter(), self::matchDigit()]);
    }

    /**
     * Return a parser that matches all character except space
     * TODO: Add all missing characters
     * @return Closure
     */
    public static function matchAllCharacterExceptSpace() {
        return self::oneOf(self::matchAlphaNumeric(), self::oneOfLiteral(...["%", "_", "-"]));
    }

    /**
     * At least the parser must match one time
     * @param $parser
     * @return Closure
     */
    public static function oneOrMore($parser) {
        return self::repeat($parser, self::MODIFIER_AT_LEAST_ONE);
    }

    /**
     * At most the parser must match one time
     * @param $parser
     * @return Closure
     */
    public static function atMostOne($parser) {
        return self::repeat($parser, self::MODIFIER_AT_MOST_ONE);
    }

    /**
     * @param $results
     * @param $modifier
     * @return boolean
     */
    private static function validateRepetition($results, $modifier) {

        switch ($modifier) {
            case self::MODIFIER_ALL:
                return true;
            case self::MODIFIER_AT_LEAST_ONE:
                return count($results) > 0;
            case self::MODIFIER_AT_MOST_ONE:
                return count($results) < 2;
            default:
                // not implemented yet
                return true;
        }
    }

    /**
     * Return a parser that is the recursive application of the parser until fail
     * @param $parser
     * @param string $modifier
     * @return Closure
     */
    public static function repeat($parser, $modifier = self::MODIFIER_ALL) {

        $counter = null;
        switch ($modifier) {
            case self::MODIFIER_AT_LEAST_ONE:
            case self::MODIFIER_ALL:
                break;
            case self::MODIFIER_AT_MOST_ONE:
                $counter = 1;
                break;
            default:
                // not implemented yet
                $counter = $modifier;
                break;
        }

        return function ($string) use ($parser, $modifier, $counter){
            $right = $string;
            $results = [];
            while (strlen($right) > 0 ) {
                $result = self::run($parser, $right);

                if ($result instanceof Ok) {

                    $results[] = $result->get()->left();
                    $right = $result->get()->right();
                    $counter--;

                    if (!is_null($counter) && $counter < 1) {
                        break;
                    }

                } else {

                    break;
                }

            }

            if (!self::validateRepetition($results, $modifier)) {
                return Result::error();
            }

            return Result::ok(new Pair($results, $right));
        };
    }

    /**
     * Runs a parser with the specified string
     * @param $parser
     * @param $string
     * @return Ok|Error
     */
    public static function run($parser, $string) {
        return $parser($string);
    }

    /**
     * Create a parser that detects whether the fist character of the string is the character to found
     * @param $toFound
     * @return Closure
     */
    public static function matchLiteral($toFound) {
        /**
         * @param $string
         * @return Ok|Error
         */
        return function($string) use($toFound) {
            if(substr_compare($string, $toFound, 0, strlen($toFound)) === 0) {
                $right = substr($string, strlen($toFound));
                return Result::ok(new Pair(strval($toFound), $right));
            } else {
                return Result::error(sprintf("Expecting <%s>, got <%s>", $toFound, substr($string, 0, strlen($toFound))));
            }
        };
    }

    /**
     * Create a parser that detects whether the regex matches the string
     * @param $regex
     * @return Closure
     */
    public static function matchRegex($regex) {

        /**
         * @param $string
         * @return Ok|Error
         */
        return function($string) use ($regex) {

            if (preg_match($regex, $string, $matches ) === 1) {
                $left = $matches[0];
                $right = substr($string, strlen($left));
                return Result::ok(new Pair($left, $right));
            } else {
                return Result::error(sprintf("Expecting to match the regex <%s> for <%s>, no match found", $regex, $string));
            }

        };
    }
}