<?php


namespace Noa\Parser;


use Closure;
use Noa\Parser\Components\ComponentInterface;
use Noa\Parser\Components\Filter;
use Noa\Parser\Components\Group;
use Noa\Parser\Components\InGroup;
use Noa\Parser\Components\LogicalOperator;
use Noa\Parser\Components\Property;
use Noa\Parser\Components\Value;
use Noa\Parser\Components\ValueOperator;

class GrammarCql
{

    private static function cleanupNonComponentValues($input) {

        return array_values(array_filter($input, function ($element) {return $element instanceof ComponentInterface;}));
    }

    /**
     * Create a parser that detects logical operator
     * - AND
     * - OR
     * @return Closure
     */
    public static function matchLogicalOperator() {

        return function ($string) {
            $result = Parser::run(Parser::matchRegex("/^(AND|OR)/"), $string);
            if ($result instanceof Ok) {
                $left = $result->get()->left();
                $value = new LogicalOperator($left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }
            return $result;
        };
    }

    /**
     * Create a parser that detects a property
     * - @something
     * @return Closure
     */
    public static function matchProperty() {
        return function ($string) {
            $result = Parser::run(Parser::matchRegex("/^(@[a-z_]+)/"), $string);
            if ($result instanceof Ok) {
                $left = $result->get()->left();
                $value = new Property($left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }
            return $result;
        };
    }

    /**
     * Create a parser that detects an value operator
     * - =
     * - <
     * - >
     * - <=
     * - >=
     * - <=>
     * @return Closure
     */
    public static function matchValueOperator() {

        return function ($string) {
            $result = Parser::run(Parser::matchRegex("/^<=>|<=|<|>=|>|=/"), $string);
            if ($result instanceof Ok) {
                $result = Parser::concatStrings($result);
                $left = $result->get()->left();
                $value = new ValueOperator($left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }
            return $result;
        };
    }

    /**
     * Create a parser that detects a IN operator group
     * - IN ( 12, 45, 79 )
     * - IN ( 12, 45 )
     * - IN ( 12 )
     */
    public static function matchInGroup() {
        $parserStart = Parser::matchLiteral("IN ( ");
        $multiValueComponents = [
            Parser::matchLiteral(', '),
            self::matchValue(),
        ];
        $parserMultiValues = Parser::repeat(Parser::allOf(...$multiValueComponents));
        $parserEnd = Parser::matchLiteral(" )");
        $parser =  Parser::allOf(...[
           $parserStart,
           self::matchValue(),
           $parserMultiValues,
           $parserEnd
        ]);

        return function ($string) use($parser) {

            $result = Parser::run($parser, $string);

            if ($result instanceof Ok) {

                $left = self::cleanupNonComponentValues($result->get()->left());
                $value = new InGroup($left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }

            return $result;
        };
    }

    /**
     * Create a parser that detects filter value
     * - 12
     * - test
     * - my%20test
     * - my_test
     * - my-test
     * - 12--55
     */
    public static function matchValue() {
        $parser = Parser::matchRegex("/^([^ ^,]+)/");

        return function ($string) use ($parser) {
            $result = Parser::run($parser, $string);
            if($result instanceof Ok) {
                $result = Parser::concatStrings($result);
                $left = $result->get()->left();
                $value = new Value($left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }
            return $result;
        };
    }

    /**
     * Create a parser that detects a filter group
     * - @my_property = value
     * - @my_property > 100
     * - @my_property < 100
     * - @my_property >= 100
     * - @my_property <= 100
     * - @interval <=> 100--150
     * - @my_property IN ( 12, 25 )
     */
    public static function matchFilter() {

        $parserValueGroup = Parser::allOf(...[
            self::matchValueOperator(),
            Parser::matchLiteral(' '),
            self::matchValue()
        ]);

        $parser =  Parser::allOf(...[
            self::matchProperty(),
            Parser::matchLiteral(' '),
            Parser::oneOf(...[
                $parserValueGroup,
                self::matchInGroup()
            ])
        ]);

        return function ($string) use ($parser) {
            $result = Parser::run($parser, $string);
            if($result instanceof Ok) {

                $left = self::cleanupNonComponentValues($result->get()->left());
                $value = new Filter(...$left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }
            return $result;
        };

    }

    /**
     * Match a complete expression as nested as wanted
     * - @merchant = 12 AND ( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND @price > 600 ) )
     * @return Closure
     */
    public static function matchExpression() {

        $parenthesisedGroup = [
            Parser::matchLiteral(" "),
            self::matchLogicalOperator(),
            Parser::matchLiteral(" "),
            self::matchExpressionGroup(),
        ];

        $parserParenthesised  = Parser::repeat(Parser::allOf(...$parenthesisedGroup));

        $parser =  Parser::allOf(...[
            self::matchExpressionGroup(),
            $parserParenthesised,
        ]);

        return function ($string) use ($parser) {
            $result = Parser::run($parser, $string);

            if ($result instanceof Ok) {

                $left = self::cleanupNonComponentValues($result->get()->left());
                return Result::ok(new Pair($left, $result->get()->right()));
            }

            return $result;
        };
    }

    /**
     * - @my_property = 12
     * - @my_property = 12 AND @interval <=> 100-150
     * - @my_property = 12 AND @interval <=> 100-150 OR @value IN ( 15, 56 )
     *
     * @return Closure
     */
    public static function matchFilterGroup() {

        $multiValueComponents = [
            Parser::matchLiteral(' '),
            self::matchLogicalOperator(),
            Parser::matchLiteral(' '),
            self::matchFilter(),
        ];
        $parserMultiValues = Parser::repeat(Parser::allOf(...$multiValueComponents));
        $parser = Parser::allOf(...[
            self::matchFilter(),
            $parserMultiValues,
        ]);

        return function ($string) use ($parser) {
            $result = Parser::run($parser, $string);

            if ($result instanceof Ok) {

                $left = self::cleanupNonComponentValues($result->get()->left());
                return Result::ok(new Pair($left, $result->get()->right()));
            }

            return $result;
        };
    }


    /**
     * Match an expression group
     * - @brand = Samsung AND @price < 800 OR @price > 600
     * - ( @brand = Samsung AND @price < 800 OR @price > 600 )
     * @return Closure
     */
    public static function matchExpressionGroup() {

        return Parser::oneOf(...[
           self::matchFilterGroup(),
           self::matchParenthesisedGroup()
        ]);
    }

    /**
     * Match a parenthesised expression
     * https://stackoverflow.com/a/19863847
     * - ( Just some information value_12 => 40 )
     * @return Closure
     */
    public static function matchParenthesisedGroup() {
        $parser = Parser::matchRegex("/\((([^()]|(?R))*)\)/");

        return function ($string) use ($parser) {

            $result = Parser::run($parser, $string);

            if ($result instanceof Ok) {
                $left = $result->get()->left();
                $value = new Group($left);
                return Result::ok(new Pair($value, $result->get()->right()));
            }

            return $result;
        };
    }
}