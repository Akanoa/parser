<?php


namespace Noa\Parser\Components;


interface ComponentInterface {

    public function __toString();
}