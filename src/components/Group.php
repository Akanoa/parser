<?php


namespace Noa\Parser\Components;


use Noa\Parser\GrammarCql;
use Noa\Parser\Parser;

class Group implements ComponentInterface {

    private $raw;
    private $parsed;

    public function __construct($raw) {

        $this->raw = $raw;
        $this->parsed = Parser::run(GrammarCql::matchExpression(), substr($this->raw, 2, -2));
    }

    /**
     * @return \Noa\Parser\Error|\Noa\Parser\Ok
     */
    public function getParsed()
    {
        return $this->parsed;
    }

    public function __toString()
    {
        return $this->raw;
    }
}