<?php


namespace Noa\Parser\Components;


class LogicalOperator implements ComponentInterface {

    private $operator;

    public function __construct($operator) {
        $this->operator = $operator;
    }

    public function __toString()
    {
        return $this->operator;
    }
}