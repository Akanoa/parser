<?php


namespace Noa\Parser\Components;


class Property implements ComponentInterface {

    /**
     * @var string
     */
    private $property;

    public function __construct($property) {
        $this->property = substr($property, 1);
    }

    public function __toString() {
        return '@'.$this->property;
    }
}