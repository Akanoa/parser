<?php


namespace Noa\Parser\Components;


class InGroup implements ComponentInterface
{

    /**
     * @var array
     */
    private $values;

    public function __construct($values) {

        $this->values = $values;
    }

    public function __toString() {

        $pieces = array_map(function ($element) {
            /** @var ComponentInterface $element */
            return $element->__toString();
        }, $this->values);


        return "IN ( ".implode(", ", $pieces)." )";
    }
}