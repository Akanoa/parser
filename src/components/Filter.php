<?php


namespace Noa\Parser\Components;


class Filter implements ComponentInterface {

    private $property;
    private $operator;
    /**
     * @var string
     */
    private $value;

    public function __construct(...$parameters) {
        $this->property = $parameters[0];
        $this->operator = $parameters[1];
        $this->value = $parameters[2] ?: '';
    }

    public function __toString()
    {
        $pieces = array_map(function ($element) {
            if ($element instanceof ComponentInterface) {
                return $element->__toString();
            }
        }, [$this->property, $this->operator, $this->value]);

        return implode(" ", array_filter($pieces));
    }
}