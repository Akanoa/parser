<?php


namespace Noa\Parser;

interface Ok {
    /**
     * @return Pair|mixed
     */
    public function get();
}

interface Error {
    public function error();
}

class ConcreteOk implements Ok {

    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return Pair
     */
    public function get()
    {
        return $this->value;
    }
}

class ConcreteError implements Error {

    private $err;

    public function __construct($error)
    {
        $this->err = $error;
    }

    public function error()
    {
        return $this->err;
    }
}

class Pair {


    /**
     * @var mixed
     */
    private $_left;

    /**
     * @var mixed
     */
    private $_right;

    public function __construct($left, $right)
    {
        $this->_left = $left;
        $this->_right = $right;
    }

    /**
     * @return mixed
     */
    public function left()
    {
        return $this->_left;
    }

    /**
     * @return mixed
     */
    public function right()
    {
        return $this->_right;
    }
}

final class Result
{
    public static function error($error = "") {

        return new ConcreteError($error);
    }

    public static function ok($value) {

        return new ConcreteOk($value);
    }
}