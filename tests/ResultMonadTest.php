<?php

namespace Noa\Parser\Test;

use Noa\Parser\Ok;
use Noa\Parser\Result;
use Noa\Parser\Error;
use PHPUnit\Framework\TestCase;

class ResultMonadTest extends TestCase
{

    public function testGetNothing()
    {
        $a = Result::error();
        self::assertInstanceOf(Error::class, $a);
    }

    public function testGetJust()
    {
        $a = Result::ok(12);
        self::assertInstanceOf(Ok::class, $a);
        self::assertEquals(12, $a->get());
    }
}
