<?php

namespace Noa\Parser\Test;

use Noa\Parser\Components\Filter;
use Noa\Parser\Components\Group;
use Noa\Parser\Components\LogicalOperator;
use Noa\Parser\GrammarCql;
use Noa\Parser\Parser;
use Noa\Parser\ReversePolishNotation;
use PHPUnit\Framework\TestCase;

class ReversePolishNotationTest extends TestCase
{

    public function testFrom()
    {
        // RPN = @merchant = 12
        $string = "@merchant = 12";
        $result = Parser::run(GrammarCql::matchExpression(), $string);
        $rpn = ReversePolishNotation::from($result->get());
        self::assertIsArray($rpn);
        self::assertEquals(1, count($rpn));
        self::assertInstanceOf(Filter::class, $rpn[0]);

        // RPN = @merchant = 12 @application = test AND
        $string = "@merchant = 12 AND @application = test";
        $result = Parser::run(GrammarCql::matchExpression(), $string);
        $rpn = ReversePolishNotation::from($result->get());
        self::assertIsArray($rpn);
        self::assertEquals(3, count($rpn));
        self::assertInstanceOf(Filter::class, $rpn[0]);
        self::assertInstanceOf(Filter::class, $rpn[1]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[2]);

        // RPN = @merchant = 12 @test = 42 RPN_G1 AND AND
        // RPM_G1 = @brand = Samsung @price < 800 OR
        // RPN = @merchant = 12 @test = 42 @brand = Samsung @price < 800 OR AND AND
        $string = "@merchant = 12 AND @test = 42 AND ( @brand = Samsung OR @price < 800 )";
        $result = Parser::run(GrammarCql::matchExpression(), $string);
        $rpn = ReversePolishNotation::from($result->get());
        self::assertIsArray($rpn);
        self::assertEquals(7, count($rpn));
        self::assertInstanceOf(Filter::class, $rpn[0]);
        self::assertInstanceOf(Filter::class, $rpn[1]);
        self::assertInstanceOf(Filter::class, $rpn[2]);
        self::assertInstanceOf(Filter::class, $rpn[3]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[4]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[5]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[6]);

        // RPN = @merchant = 12 RPN_G1 @application = test AND AND
        // RPN_G1 = RPN_G11 RPN_G12 OR
        // RPN_G11 = @name = Iphone%20 @price > 1000 AND
        // RPN_G12 = @brand = Samsung @price < 800 RPN_G121 AND AND
        // RPN_G121 = @price > 600 @tag IN ( 12, 42 ) OR
        // RPN_G12 = @brand = Samsung @price < 800 @price > 600 @tag IN ( 12, 42 ) OR AND AND
        // RPN_G1 = @name = Iphone%20 @price > 1000 AND @brand = Samsung @price < 800 @price > 600 @tag IN ( 12, 42 ) OR AND AND OR
        // RPN = @merchant = 12 @name = Iphone%20 @price > 1000 AND @brand = Samsung @price < 800 @price > 600 @tag IN ( 12, 42 ) OR AND AND OR @application = test AND AND
        $string = "@merchant = 12 AND ( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND ( @price > 600 OR @tag IN ( 12, 42 ) ) ) ) AND @application = test";
        $result = Parser::run(GrammarCql::matchExpression(), $string);
        $rpn = ReversePolishNotation::from($result->get());
        self::assertIsArray($rpn);
        self::assertEquals(15, count($rpn));
        self::assertInstanceOf(Filter::class, $rpn[0]);
        self::assertInstanceOf(Filter::class, $rpn[1]);
        self::assertInstanceOf(Filter::class, $rpn[2]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[3]);
        self::assertInstanceOf(Filter::class, $rpn[4]);
        self::assertInstanceOf(Filter::class, $rpn[5]);
        self::assertInstanceOf(Filter::class, $rpn[6]);
        self::assertInstanceOf(Filter::class, $rpn[7]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[8]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[9]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[10]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[11]);
        self::assertInstanceOf(Filter::class, $rpn[12]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[13]);
        self::assertInstanceOf(LogicalOperator::class, $rpn[14]);
    }
}
