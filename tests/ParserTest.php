<?php

namespace Noa\Parser\Test;

use Noa\Parser\Ok;
use Noa\Parser\Result;
use Noa\Parser\Error;
use Noa\Parser\Parser;
use Noa\Parser\Pair;
use PHPUnit\Framework\TestCase;

/**
 * @property Parser parser
 */
class ParserTest extends TestCase
{

    protected function setUp(): void
    {
        $this->parser = new Parser();
    }

//    public function testSimpleRequest()
//    {
//        $query = "@stock = 1";
//        $expected = [
//            "filters" => [
//                "filters" => [
//                    [
//                        [
//                            "raw" => "@stock = 1",
//                            "property" => "@stock",
//                            "value" => "1",
//                            "operator" => "="
//                        ]
//                    ]
//                ],
//                "operator" => ""
//            ]
//        ];
//    }

    public function testMatchLiteralFound() {
        $string = "ABC";
        $result = Parser::run(Parser::matchLiteral("A"), $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("A", $result->get()->left());
        self::assertEquals("BC", $result->get()->right());

        $result = Parser::run(Parser::matchLiteral("AB"), $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("AB", $result->get()->left());
        self::assertEquals("C", $result->get()->right());
    }

    public function testMatchLiteralNotFound() {
        $string = "ZBC";
        $result = Parser::run(Parser::matchLiteral("A"), $string);
        self::assertInstanceOf(Error::class, $result);
        self::assertEquals($result->error(), "Expecting <A>, got <Z>");
    }

    public function testMatchRegexFound() {
        $re = '/^([^@\s<&>]+)@(?:([-a-z0-9]+)\.)+([a-z]{2,})/iD';
        $string = 'truc@domaine.com';
        $result = Parser::run(Parser::matchRegex($re), $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($string, $result->get()->left());
        self::assertEquals("", $result->get()->right());

        $re = '/^([^@\s<&>]+)@(?:([-a-z0-9]+)\.)+([a-z]{2,})/iD';
        $string = 'truc@domaine.com carry';
        $result = Parser::run(Parser::matchRegex($re), $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("truc@domaine.com", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());
    }

    public function testMatchRegexNotFound() {
        $re = '/^([^@\s<&>]+)@(?:([-a-z0-9]+)\.)+([a-z]{2,})/iD';
        $string = 'http://g-rossolini.developpez.com/';
        $result = Parser::run(Parser::matchRegex($re), $string);
        self::assertInstanceOf(Error::class, $result);
        self::assertEquals($result->error(), "Expecting to match the regex <$re> for <$string>, no match found");

    }

    public function testOrElseFound() {
        $parserA = Parser::matchLiteral("A");
        $parserB = Parser::matchLiteral("B");
        $parserAorB = Parser::orElse($parserA, $parserB);

        $string1 = "ABC";
        $result1 =  Parser::run($parserAorB, $string1);
        self::assertInstanceOf(Ok::class, $result1);
        self::assertEquals("A", $result1->get()->left());
        self::assertEquals("BC", $result1->get()->right());

        $string2 = "BBC";
        $result2 =  Parser::run($parserAorB, $string2);
        self::assertInstanceOf(Ok::class, $result2);
        self::assertEquals("A", $result1->get()->left());
        self::assertEquals("BC", $result1->get()->right());
    }

    public function testOrElseNotFound() {
        $parserA = Parser::matchLiteral("A");
        $parserB = Parser::matchLiteral("B");
        $parserAorB = Parser::orElse($parserA, $parserB);

        $string1 = "ZBC";
        $result1 =  Parser::run($parserAorB, $string1);
        self::assertInstanceOf(Error::class, $result1);

        $string2 = "ZBC";
        $result2 =  Parser::run($parserAorB, $string2);
        self::assertInstanceOf(Error::class, $result2);
    }

    public function testAndThenFound() {
        $parserA = Parser::matchLiteral("A");
        $parserB = Parser::matchLiteral("B");
        $parserAandB = Parser::andThen($parserA, $parserB);

        $string = "ABC";
        $result = Parser::run($parserAandB, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals(["A","B"], $result->get()->left());
        self::assertEquals("C", $result->get()->right());

    }

    public function testAndThenNotFound() {
        $parserA = Parser::matchLiteral("A");
        $parserB = Parser::matchLiteral("B");
        $parserAandB = Parser::andThen($parserA, $parserB);

        $string1 = "ZBC";
        $result1 = Parser::run($parserAandB, $string1);
        self::assertInstanceOf(Error::class, $result1);

        $string2 = "AZC";
        $result2 = Parser::run($parserAandB, $string2);
        self::assertInstanceOf(Error::class, $result2);

    }

    public function testConvertToIntSuccess() {
        $string = "123";
        $result1 = Parser::toInt(Result::ok($string));
        self::assertInstanceOf(Ok::class, $result1);
        self::assertIsInt($result1->get());
        self::assertEquals(123, $result1->get());

        $string = "123";
        $result2 = Parser::toInt(Result::ok(new Pair($string, "")));
        self::assertInstanceOf(Ok::class, $result2);
        self::assertIsInt($result2->get()->left());
        self::assertEquals(123, $result2->get()->left());
    }

    public function testConvertToIntNotSuccess() {
        $string = "a2c";
        $result1 = Parser::toInt(Result::ok($string));
        self::assertInstanceOf(Error::class, $result1);

        $string = "a2c";
        $result2 = Parser::toInt(Result::ok(new Pair($string, "")));
        self::assertInstanceOf(Error::class, $result2);

        $result3 = Parser::toInt(Result::error());
        self::assertInstanceOf(Error::class, $result3);
    }

    public function testConvertToFloatSuccess() {
        $string = "123";
        $result1 = Parser::toFloat(Result::ok($string));
        self::assertInstanceOf(Ok::class, $result1);
        self::assertIsFloat($result1->get());
        self::assertEquals(123.0, $result1->get());

        $string = "123";
        $result2 = Parser::toFloat(Result::ok(new Pair($string, "")));
        self::assertInstanceOf(Ok::class, $result2);
        self::assertIsFloat($result2->get()->left());
        self::assertEquals(123.0, $result2->get()->left());
    }

    public function testConvertToFloatNotSuccess() {
        $string = "a2c";
        $result1 = Parser::toFloat(Result::ok($string));
        self::assertInstanceOf(Error::class, $result1);

        $string = "a2c";
        $result2 = Parser::toFloat(Result::ok(new Pair($string, "")));
        self::assertInstanceOf(Error::class, $result2);

        $result3 = Parser::toFloat(Result::error());
        self::assertInstanceOf(Error::class, $result3);
    }

    public function testConcatStringsSuccess() {
        $input1 = ["a", "b", "c"];
        $result1 = Parser::concatStrings(Result::ok($input1));
        self::assertInstanceOf(Ok::class, $result1);
        self::assertIsString($result1->get());
        self::assertEquals("abc", $result1->get());

        $input2 = ["a", "b", "c"];
        $result2 = Parser::concatStrings(Result::ok(new Pair($input2, "")));
        self::assertInstanceOf(Ok::class, $result2);
        self::assertIsString($result2->get()->left());
        self::assertEquals("abc", $result2->get()->left());

        $input2 = [];
        $result2 = Parser::concatStrings(Result::ok(new Pair($input2, "")));
        self::assertInstanceOf(Ok::class, $result2);
        self::assertIsString($result2->get()->left());
        self::assertEquals("", $result2->get()->left());

        $input2 = ["a", ["b","c"]];
        $result2 = Parser::concatStrings(Result::ok(new Pair($input2, "")));
        self::assertInstanceOf(Ok::class, $result2);
        self::assertIsString($result2->get()->left());
        self::assertEquals("abc", $result2->get()->left());
    }

    public function testConcatStringsFailure() {
        // Not an array
        $input1 = false;
        $result1 = Parser::concatStrings(Result::ok($input1));
        self::assertInstanceOf(Error::class, $result1);

        // One of the element isn't a string
        $input2 = ["a", 1, "c"];
        $result2 = Parser::concatStrings(Result::ok(new Pair($input2, "")));
        self::assertInstanceOf(Error::class, $result2);

        $result3 = Parser::concatStrings(Result::error());
        self::assertInstanceOf(Error::class, $result3);

    }

    public function testApplyToIntSuccess() {
        $string = "12C";
        $parser1 = Parser::matchLiteral("1");
        $parser2 = Parser::matchLiteral("2");
        $parser1and2 = Parser::andThen($parser1, $parser2);
        $parser1and2Concat = Parser::apply($parser1and2, [Parser::class, 'concatStrings']);
        $parser1and2ToInt = Parser::apply($parser1and2Concat, [Parser::class, 'toInt']);
        $result = Parser::run($parser1and2ToInt, $string);

        self::assertInstanceOf(Ok::class, $result);
        self::assertIsInt($result->get()->left());
        self::assertEquals(12, $result->get()->left());
    }

    public function testApplyToIntFailure() {
        $string = "A2C";
        $parser1 = Parser::matchLiteral("1");
        $parser2 = Parser::matchLiteral("2");
        $parser1and2 = Parser::andThen($parser1, $parser2);
        $parser1and2Concat = Parser::apply($parser1and2, [Parser::class, 'concatStrings']);
        $result = Parser::run($parser1and2Concat, $string);

        self::assertInstanceOf(Error::class, $result);

    }

    public function testAllOfFound() {

        $string = "123456";
        $parser = Parser::allOfLiteral();
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals([], $result->get()->left());
        self::assertEquals("123456", $result->get()->right());

        $parser = Parser::allOfLiteral('1');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('1', $result->get()->left());
        self::assertEquals("23456", $result->get()->right());

        $parser = Parser::allOfLiteral('1', '2');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals(['1', '2'], $result->get()->left());
        self::assertEquals("3456", $result->get()->right());

        $parser = Parser::allOfLiteral('1', '2', '3');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals(['1', '2', '3'], $result->get()->left());
        self::assertEquals("456", $result->get()->right());

        $parser = Parser::allOfLiteral('1', '2', '3', '4');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals(['1', '2', '3', '4'], $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $parser = Parser::allOfLiteral(...['1', '2', '3', '4']);
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals(['1', '2', '3', '4'], $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $string = "Hello World!";
        $parser = Parser::allOfLiteral("Hello", " ","World");
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals(["Hello", " ",  "World"], $result->get()->left());
        self::assertEquals("!", $result->get()->right());
    }

    public static function testAllOfNotFound() {

        $string = "z23456";
        $parser = Parser::allOfLiteral('1', '2');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Error::class, $result);

    }

    public function testOneOfFound() {
        $string = "456";

        $parser = Parser::oneOfLiteral();
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals([], $result->get()->left());
        self::assertEquals("456", $result->get()->right());

        $parser = Parser::oneOfLiteral('4');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('4', $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $parser = Parser::oneOfLiteral('1', '2', '3', '4');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('4', $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $string = "Hello World!";
        $parser = Parser::oneOfLiteral("Hola", "Hello", "Hi");
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("Hello", $result->get()->left());
        self::assertEquals(" World!", $result->get()->right());
    }

    public static function testOneOfNotFound() {

        $string = "456";
        $parser = Parser::oneOfLiteral('1', '2', '3');
        $result = Parser::run($parser, $string);
        self::assertInstanceOf(Error::class, $result);

    }

    public function testMatchDigitFound() {

        $result = Parser::run(Parser::matchDigit(), "456");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('4', $result->get()->left());
        self::assertEquals("56", $result->get()->right());
    }

    public function testMatchDigitNotFound() {

        $result = Parser::run(Parser::matchDigit(), "x56");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(Parser::matchDigit(), " 56");
        self::assertInstanceOf(Error::class, $result);
    }

    public function testMatchLowerCaseLetterFound() {

        $result = Parser::run(Parser::matchLowerCaseLetter(), "j56");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('j', $result->get()->left());
        self::assertEquals("56", $result->get()->right());
    }

    public function testMatchLowerCaseLetterNotFound() {

        $result = Parser::run(Parser::matchLowerCaseLetter(), "156");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(Parser::matchLowerCaseLetter(), "A56");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(Parser::matchLowerCaseLetter(), " 56");
        self::assertInstanceOf(Error::class, $result);
    }

    public function testMatchUpperCaseLetterFound() {

        $result = Parser::run(Parser::matchUpperCaseLetter(), "J56");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('J', $result->get()->left());
        self::assertEquals("56", $result->get()->right());
    }

    public function testMatchUpperCaseLetterNotFound() {

        $result = Parser::run(Parser::matchUpperCaseLetter(), "156");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(Parser::matchUpperCaseLetter(), "a56");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(Parser::matchUpperCaseLetter(), " 56");
        self::assertInstanceOf(Error::class, $result);
    }

    public function testMatchLetterFound() {

        $result = Parser::run(Parser::matchLetter(), "J56");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('J', $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $result = Parser::run(Parser::matchLetter(), "j56");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('j', $result->get()->left());
        self::assertEquals("56", $result->get()->right());
    }

    public function testMatchLetterNotFound() {

        $result = Parser::run(Parser::matchLetter(), "156");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(Parser::matchLetter(), " 56");
        self::assertInstanceOf(Error::class, $result);
    }

    public function testMatchAlphaNumericFound() {

        $result = Parser::run(Parser::matchAlphaNumeric(), "J56");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('J', $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $result = Parser::run(Parser::matchAlphaNumeric(), "j56");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('j', $result->get()->left());
        self::assertEquals("56", $result->get()->right());

        $result = Parser::run(Parser::matchAlphaNumeric(), "456");
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals('4', $result->get()->left());
        self::assertEquals("56", $result->get()->right());
    }

    public function testMatchAlphaNumericNotFound() {

        $result = Parser::run(Parser::matchAlphaNumeric(), " 56");
        self::assertInstanceOf(Error::class, $result);
    }

    public function testRepeatFound() {

        $parser = Parser::repeat(Parser::matchAlphaNumeric());
        $result = Parser::run($parser, "aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("aB12", $result->get()->left());
        self::assertEquals(" u", $result->get()->right());

        $parser = Parser::repeat(Parser::matchAlphaNumeric(), Parser::MODIFIER_AT_LEAST_ONE);
        $result = Parser::run($parser, "aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("aB12", $result->get()->left());
        self::assertEquals(" u", $result->get()->right());

        $parser = Parser::repeat(Parser::matchAlphaNumeric(), Parser::MODIFIER_AT_MOST_ONE);
        $result = Parser::run($parser, "aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("a", $result->get()->left());
        self::assertEquals("B12 u", $result->get()->right());

        $parser = Parser::repeat(Parser::matchAlphaNumeric(), Parser::MODIFIER_AT_MOST_ONE);
        $result = Parser::run($parser, " aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("", $result->get()->left());
        self::assertEquals(" aB12 u", $result->get()->right());
    }

    public function testRepeatNotFound() {

        $parser = Parser::repeat(Parser::matchAlphaNumeric(), Parser::MODIFIER_AT_LEAST_ONE);
        $result = Parser::run($parser, "   ");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Error::class, $result);

    }

    public function testOneOrMoreFound() {

        $parser = Parser::oneOrMore(Parser::matchAlphaNumeric());
        $result = Parser::run($parser, "aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("aB12", $result->get()->left());
        self::assertEquals(" u", $result->get()->right());
    }

    public function testOneOrMoreNotFound() {

        $parser = Parser::oneOrMore(Parser::matchAlphaNumeric());
        $result = Parser::run($parser, "   ");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Error::class, $result);
    }

    public function testAtMostOneFound() {

        $parser = Parser::atMostOne(Parser::matchAlphaNumeric());
        $result = Parser::run($parser, "aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("a", $result->get()->left());
        self::assertEquals("B12 u", $result->get()->right());

        $parser = Parser::atMostOne(Parser::matchAlphaNumeric());
        $result = Parser::run($parser, " aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("", $result->get()->left());
        self::assertEquals(" aB12 u", $result->get()->right());
    }

    public function testMatchAllCharactersExceptSpace() {

        $parser = Parser::atMostOne(Parser::matchAllCharacterExceptSpace());
        $result = Parser::run($parser, "aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("a", $result->get()->left());
        self::assertEquals("B12 u", $result->get()->right());

        $parser = Parser::atMostOne(Parser::matchAllCharacterExceptSpace());
        $result = Parser::run($parser, "-aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("-", $result->get()->left());
        self::assertEquals("aB12 u", $result->get()->right());

        $parser = Parser::atMostOne(Parser::matchAllCharacterExceptSpace());
        $result = Parser::run($parser, "_aB12 u");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals("_", $result->get()->left());
        self::assertEquals("aB12 u", $result->get()->right());
    }
}
