<?php

namespace Noa\Parser\Test;

use Noa\Parser\Components\ComponentInterface;
use Noa\Parser\Components\Filter;
use Noa\Parser\Components\Group;
use Noa\Parser\Components\InGroup;
use Noa\Parser\Components\LogicalOperator;
use Noa\Parser\Components\Property;
use Noa\Parser\Components\Value;
use Noa\Parser\Components\ValueOperator;
use Noa\Parser\Error;
use Noa\Parser\GrammarCql;
use Noa\Parser\Ok;
use Noa\Parser\Parser;
use PHPUnit\Framework\TestCase;

/**
 * @param ComponentInterface|array $input
 * @return string
 */
function asString($input) {

    if ($input instanceof  ComponentInterface) {
        return $input->__toString();
    }

    $result = array_map(function ($element) {
        if ($element instanceof ComponentInterface) {
            return $element->__toString();
        }
    }, $input);

    return implode(" ", array_values(array_filter($result)));
}

class GrammarCqlTest extends TestCase
{

    public function testMatchLogicalOperatorFound()
    {
        $result = Parser::run(GrammarCql::matchLogicalOperator(), "AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(LogicalOperator::class, $result->get()->left());
        self::assertEquals("AND", $result->get()->left());
        self::assertEquals("", $result->get()->right());

        $result = Parser::run(GrammarCql::matchLogicalOperator(), "AND something");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(LogicalOperator::class, $result->get()->left());
        self::assertEquals("AND", $result->get()->left());
        self::assertEquals(" something", $result->get()->right());

        $result = Parser::run(GrammarCql::matchLogicalOperator(), "OR");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(LogicalOperator::class, $result->get()->left());
        self::assertEquals("OR", $result->get()->left());
        self::assertEquals("", $result->get()->right());
    }

    public function testMatchLogicalOperatorNotFound()
    {

        // it isn't "AND" or "OR"
        $result = Parser::run(GrammarCql::matchLogicalOperator(), "test");
        self::assertInstanceOf(Error::class, $result);

        // "AND" is in lower case
        $result = Parser::run(GrammarCql::matchLogicalOperator(), "and");
        self::assertInstanceOf(Error::class, $result);
    }

    public function testMatchPropertyFound() {

        $result = Parser::run(GrammarCql::matchProperty(), "@my_property = 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Property::class, $result->get()->left());
        self::assertEquals("@my_property", $result->get()->left());
        self::assertEquals(" = 12", $result->get()->right());

    }

    public function testMatchPropertyNotFound() {

        $result = Parser::run(GrammarCql::matchProperty(), "my_property = 12");
        self::assertInstanceOf(Error::class, $result);

        $result = Parser::run(GrammarCql::matchProperty(), "@MY_PROPERTY = 12");
        self::assertInstanceOf(Error::class, $result);

    }

    public function testMatchValueOperatorFound() {

        $result = Parser::run(GrammarCql::matchValueOperator(), "= 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(ValueOperator::class, $result->get()->left());
        self::assertEquals("=", $result->get()->left());
        self::assertEquals(" 12", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValueOperator(), "< 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(ValueOperator::class, $result->get()->left());
        self::assertEquals("<", $result->get()->left());
        self::assertEquals(" 12", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValueOperator(), "> 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(ValueOperator::class, $result->get()->left());
        self::assertEquals(">", $result->get()->left());
        self::assertEquals(" 12", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValueOperator(), ">= 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(ValueOperator::class, $result->get()->left());
        self::assertEquals(">=", $result->get()->left());
        self::assertEquals(" 12", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValueOperator(), "<= 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(ValueOperator::class, $result->get()->left());
        self::assertEquals("<=", $result->get()->left());
        self::assertEquals(" 12", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValueOperator(), "<= 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(ValueOperator::class, $result->get()->left());
        self::assertEquals("<=", $result->get()->left());
        self::assertEquals(" 12", $result->get()->right());
    }

    public function testMatchOperatorNotFound() {
        $result = Parser::run(GrammarCql::matchValueOperator(), "?? 12");
        $result = Parser::concatStrings($result);
        self::assertInstanceOf(Error::class, $result);
    }

    public function testMatchValueFound() {
        $result = Parser::run(GrammarCql::matchValue(), "12 carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("12", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValue(), "12, carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("12", $result->get()->left());
        self::assertEquals(", carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValue(), "test carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("test", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValue(), "my%20test carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("my%20test", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValue(), "my_test carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("my_test", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValue(), "my-test carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("my-test", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchValue(), "12--55 carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Value::class, $result->get()->left());
        self::assertEquals("12--55", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());
    }

    public static function testMatchInGroup() {
        $result = Parser::run(GrammarCql::matchInGroup(), "IN ( 12 ) carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(InGroup::class, $result->get()->left());
        self::assertEquals("IN ( 12 )", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchInGroup(), "IN ( 12, 45 ) carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(InGroup::class, $result->get()->left());
        self::assertEquals("IN ( 12, 45 )", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());

        $result = Parser::run(GrammarCql::matchInGroup(), "IN ( 12, 45, 79 ) carry");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(InGroup::class, $result->get()->left());
        self::assertEquals("IN ( 12, 45, 79 )", $result->get()->left());
        self::assertEquals(" carry", $result->get()->right());
    }

    public function testMatchFilter() {
        $result = Parser::run(GrammarCql::matchFilter(), "@my_property = value AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@my_property = value", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilter(), "@my_property > 100 AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@my_property > 100", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilter(), "@my_property < 100 AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@my_property < 100", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilter(), "@my_property <= 100 AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@my_property <= 100", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilter(), "@my_property >= 100 AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@my_property >= 100", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilter(), "@interval <=> 100--150 AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@interval <=> 100--150", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilter(), "@my_property IN ( 12, 25 ) AND");
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Filter::class, $result->get()->left());
        self::assertEquals("@my_property IN ( 12, 25 )", $result->get()->left());
        self::assertEquals(" AND", $result->get()->right());
    }

    public function testMatchFilterGroupFound() {

        $result = Parser::run(GrammarCql::matchFilterGroup(), "@my_property = 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertIsArray($result->get()->left());
        self::assertEquals("@my_property = 12", asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilterGroup(), "@interval <=> 100-150 AND @my_property = 12");
        self::assertInstanceOf(Ok::class, $result);
        self::assertIsArray($result->get()->left());
        self::assertEquals("@interval <=> 100-150 AND @my_property = 12", asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $result = Parser::run(GrammarCql::matchFilterGroup(), "@interval <=> 100-150 AND @my_property = 12 OR @value IN ( test, 12, Iphone%20 )");
        self::assertInstanceOf(Ok::class, $result);
        self::assertIsArray($result->get()->left());
        self::assertEquals("@interval <=> 100-150 AND @my_property = 12 OR @value IN ( test, 12, Iphone%20 )", asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $expression = "@brand = Samsung AND @price < 800 AND @price > 600";
        $result = Parser::run(GrammarCql::matchFilterGroup(), $expression);
        self::assertInstanceOf(Ok::class, $result);
        self::assertIsArray($result->get()->left());
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());
    }

    public function testMatchExpressionGroup() {

        $expression = "( @brand = Samsung AND @price < 800 OR @price > 600 )";
        $result = Parser::run(GrammarCql::matchExpressionGroup(), $expression);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $expression = "@brand = Samsung AND @price < 800 OR @price > 600";
        $result = Parser::run(GrammarCql::matchExpressionGroup(), $expression);
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());
    }

    public function testMatchExpression() {

        $expression = "@merchant = 12 AND @test = 42 AND ( @brand = Samsung AND @price < 800 OR @price > 600 )";
        $result = Parser::run(GrammarCql::matchExpression(), $expression);
        self::assertInstanceOf(Ok::class, $result);
        self::assertIsArray($result->get()->left());
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $expression = "( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND @price > 600 )";
        $result = Parser::run(GrammarCql::matchExpression(), $expression);
        self::assertIsArray($result->get()->left());
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $expression = "@merchant = 12 AND ( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND @price > 600 ) )";
        $result = Parser::run(GrammarCql::matchExpression(), $expression);
        self::assertIsArray($result->get()->left());
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $expression = "@merchant = 12 AND ( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND @price > 600 ) ) AND @application = test";
        $result = Parser::run(GrammarCql::matchExpression(), $expression);
        self::assertIsArray($result->get()->left());
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());

        $expression = "@merchant = 12 AND ( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND ( @price > 600 OR @tag IN ( 12, 42 ) ) ) ) AND @application = test";
        $result = Parser::run(GrammarCql::matchExpression(), $expression);
        self::assertIsArray($result->get()->left());
        self::assertInstanceOf(Ok::class, $result);
        self::assertEquals($expression, asString($result->get()->left()));
        self::assertEquals("", $result->get()->right());
    }

    public function testMatchParenthesisedGroupFound() {
        $expression = "( @name = Iphone%20X AND @price > 1000 ) and something";
        $result = Parser::run(GrammarCql::matchParenthesisedGroup(), $expression);
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Group::class, $result->get()->left());
        self::assertEquals("( @name = Iphone%20X AND @price > 1000 )", $result->get()->left());
        self::assertEquals(" and something", $result->get()->right());

        $expression = "( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND @price > 600 ) ) and something";
        $result = Parser::run(GrammarCql::matchParenthesisedGroup(), $expression);
        self::assertInstanceOf(Ok::class, $result);
        self::assertInstanceOf(Group::class, $result->get()->left());
        self::assertEquals("( ( @name = Iphone%20X AND @price > 1000 ) OR ( @brand = Samsung AND @price < 800 AND @price > 600 ) )", $result->get()->left());
        self::assertEquals(" and something", $result->get()->right());
    }
}
